import 'dart:math';

import 'package:flutter/foundation.dart';

class CartItem {
  final String id;
  final String title;
  final int quantify;
  final double price;

  CartItem({
  @required this.id,
    @required this.title,
    @required this.quantify,
    @required this.price,

  });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String,CartItem> get items{
    return{..._items};
}

int get itemCount {
    return  _items.length;
}

double get totalAmount {
    var total = 0.0;
    _items.forEach((key,cart){
      total += cart.price * cart.quantify;
    });
    return total;
}

void addItem(String productId, double price, String title,){
    if(_items.containsKey(productId)){
      _items.update(productId, (existingCartItem)=>CartItem(id: existingCartItem.id,
          title: existingCartItem.title,
          price: existingCartItem.price,
          quantify: existingCartItem.quantify+1));
    }else{
      _items.putIfAbsent(productId, ()=> CartItem(id: DateTime.now().toString(),title: title, price: price,
      quantify: 1,
      ),
      );
    }
    notifyListeners();
}

  void removeItem(String productId){
    _items.remove(productId);
    notifyListeners();
  }

  void clear(){
    _items = {};
    notifyListeners();
  }

  void removeSingleItem(String productId){
    if(!_items.containsKey(productId)){
      return;
    }
    if(_items[productId].quantify>1){
      _items.update(productId, (existingCartItem)=>CartItem(
          id: existingCartItem.id,
          title: existingCartItem.title,
          price: existingCartItem.price,
          quantify: existingCartItem.quantify-1),
      );
    }else{
      _items.remove(productId);
    }
    notifyListeners();

  }
}